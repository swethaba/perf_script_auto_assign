package main

import (
	"bufio"
	"encoding/csv"
	"github.com/Shopify/sarama"
	"gopkg.in/alecthomas/kingpin.v2"
	"os"
	"strconv"
	"strings"
	"time"
	"io"
	"github.com/fatih/color"
	"github.com/streadway/amqp"
	"log"
	"fmt"
	"math/rand"
)

func failOnError(err error, msg string) {
	if err != nil {
		color.Red("%s: %s", msg, err)
	}
}

func publishMessageInKafka(topicName string, message string){

	kafkaConn  := []string{"central-kafka-cluster-1.swiggyperf.in:9092","central-kafka-cluster-2.swiggyperf.in:9092","central-kafka-cluster-3.swiggyperf.in:9092","central-kafka-cluster-4.swiggyperf.in:9092","central-kafka-cluster-5.swiggyperf.in:9092","central-kafka-cluster-6.swiggyperf.in:9092"}
	//mytopic    := "swiggy.orders_delivery"
	brokerList := kingpin.Flag("brokerList", "List of brokers to connect").Default(kafkaConn...).Strings()
	topic      := kingpin.Flag("topic", topicName).Default(topicName).String()
	maxRetry   := kingpin.Flag("maxRetry", "Retry limit").Default("5").Int()


	kingpin.Parse()
	config := sarama.NewConfig()
	config.Producer.RequiredAcks = sarama.WaitForAll
	config.Producer.Retry.Max = *maxRetry
	config.Producer.Return.Successes = true
	producer, err := sarama.NewSyncProducer(*brokerList, config)
	if err != nil {
		fmt.Printf("aaaaaaaa================")
		panic(err)
	}
	defer func() {
		if err := producer.Close(); err != nil {
			panic(err)
		}
	}()

	msgNew := &sarama.ProducerMessage{
		Topic: *topic,
		Value: sarama.StringEncoder(message),
	}
	partition, offset, err := producer.SendMessage(msgNew)
	if err == nil {
		fmt.Printf("New order event sent to Kafka . . . . . ")
		//panic(err)
	}
	fmt.Printf(" :: Message is stored in topic(%s)/partition(%d)/offset(%d)\n", *topic, partition, offset)
}

func publishMessage(ch *amqp.Channel, message, exchange, queueName string) {
	err := ch.Publish(
		exchange,  // exchange
		queueName, // routing key
		false,     // mandatory
		false,     // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(message),
		})
	if err != nil {
		fmt.Println(err)
	}
	//log.Printf(" [x] Sent %s", message)
	failOnError(err, "Failed to publish a message")

}

func random(min int, max int) int {
	return rand.Intn(max-min) + min
}

func main() {
	csvFile, _ := os.Open("/home/centos/goworkspace/demofile.csv")
	reader := csv.NewReader(bufio.NewReader(csvFile))
	i:= 0
	max := 6000
	for i < max {
		i = publishOrder(reader, csvFile, i, max)
	}
}

func publishOrder(reader *csv.Reader, csvFile *os.File, count int, max int) int {
	for {
		if (count > max) {
			break
		}
		line, error := reader.Read()
		if error == io.EOF {
			break
		} else if error != nil {
			log.Fatal(error)
		}
		defer csvFile.Close()
		template := `{ "actual_sla_time": "0", "agreement_type": "1", "assignement_delay_pred": "0", "banner_factor": "0.0", "banner_factor_diff": "null", "batch_opt_in": "SHOWN_OPTED", "billing_address_id": "13184547", "billing_lat": "12.932559", "billing_lng": "77.603578", "cancellation_fee_actual": "0", "cancellation_fee_applied": 0, "cancellation_fee_collected": 0, "cancellation_fee_collected_total": 0, "cancellation_fee_gst_expression": "18", "cancellation_order_summary_link": "/invoice/download?token=1748279_null", "cart_id": 31008, "charges": { "Cancellation Fee": "0", "Convenience Fee": "0", "Delivery Charges": "0", "GST": "15", "Packing Charges": "0", "Service Charges": "0", "Service Tax": "0", "Vat": "0" }, "cod_verification_threshold": 1000, "commission": "15", "commission_on_full_bill": true, "configurations": { }, "convenience_fee": "0", "convenience_fee_actual": "0", "convenience_fee_gst_expression": "18", "converted_to_cod": false, "coupon_applied": "", "coupon_code": "", "coupon_discount": 0, "cust_lat_lng": { "lat": "${custlat}", "lng": "${custlon}" }, "customer_id": "6954645", "customer_ip": "10.0.15.223", "customer_user_agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3381.1 Safari/537.36", "de_pickedup_refund": 0, "delayed_placing": 0, "delivered_time_in_seconds": "0", "delivery_address": { "address": "IBC Knowledge Park, Bhavani Nagar, Suddagunte Palya, Bengaluru, Karnataka 560029, India", "address_line1": "IBC Knowledge Park, Bhavani Nagar, Suddagunte Palya, Bengaluru, Karnataka 560029, India", "annotation": "Work", "area": "Suddagunte Palya", "city": "bangalore", "email": "dhanaraj.t@swiggy.in", "flat_no": "1111 Siwggy office", "id": "13184547", "landmark": "IBC Knowledge Park", "lat": "12.932559", "lng": "77.603578", "mobile": "9008613367", "name": "Dhanaraj", "reverse_geo_code_failed": false }, "delivery_charge_actual": "0", "delivery_charge_gst_expression": "18", "delivery_time_in_seconds": "0", "device_id": "fa81c642-9852-461b-ab13-4caf03a8f496", "distance_calc_method": "HAVERSINE_LUCIFER", "distance_fee": 0, "first_mile_time_pred": "10.0", "first_order": false, "free_delivery_discount_hit": 0, "free_gifts": [ ], "free_shipping": "0", "GST_details": { "cart_CGST": 7.5, "cart_SGST": 7.5, "item_CGST": 7.5, "item_SGST": 7.5 }, "GST_on_cancellation_fee": { }, "GST_on_commission": { "CGST": 1.3500000536441803, "GST": 2.7, "SGST": 1.3500000536441803 }, "GST_on_commission_expressions": { "CGST": "0.09", "IGST": "0", "SGST": "0.09" }, "GST_on_convenience_fee": { }, "GST_on_delivery_fee": { }, "has_rating": "0", "is_assured": 0, "is_assured_restaurant": false, "is_cancellable": false, "is_coupon_applied": false, "is_ivr_enabled": "0", "is_partner_enable": true, "is_refund_initiated": 0, "is_replicated": false, "is_select": false, "item_total": 300, "key": "DKW5PF", "last_failed_order_id": 0, "last_mile_time_pred": "15.185", "listing_version_shown": "", "mCancellationTime": 0, "menu_shown": { }, "nodal_spending": "297.3", "on_time": false, "order_delivery_charge": 0, "order_delivery_status": "", "order_discount": 0, "order_id": "${orderId}", "order_incoming": "315", "order_items": [ { "added_by_user_id": -1, "added_by_username": "", "addons": [ ], "category_details": { "category": "Ice Creams", "sub_category": "Scoop With Sauce" }, "external_item_id": "", "global_main_category": "DESSERTS", "global_sub_category": "ICE CREAMS", "GST_details": { "CGST": 0, "IGST": 0, "SGST": 0 }, "image_id": "phr96evv9br5fmgjy9fq", "is_veg": "1", "item_charges": { "GST": "0", "Service Charges": "0", "Service Tax": "0", "Vat": "0" }, "item_id": 79427, "item_restaurant_discount_hit": 0, "item_swiggy_discount_hit": 0, "item_tax_expressions": { "GST_inclusive": false, "Service Charges": "0", "Service Tax": "0", "Vat": "0" }, "meal_quantity": "1", "name": "Vanilla with Butterscotch Sauce", "packing_charges": "0", "quantity": "3", "single_variant": false, "subtotal": "300", "total": "300", "variants": [ ] } ], "order_meals": [ ], "order_payment_method": "Cash", "order_placement_status": "0", "order_restaurant_bill": "600", "order_spending": "0", "order_status": "processing", "order_tax": 15, "order_time": "${time}", "order_total": 315, "order_type": "regular", "ordered_time_in_seconds": 1558698659, "original_order_total": 315, "overbooking": "0", "partner_id": "9990", "pay_by_system_value": true, "payment": "successful", "payment_confirmation_channel": "api", "payment_method": "FreeCharge", "payment_txn_id": "", "payment_txn_status": "success", "pg_charge": "0", "pg_response_time": "${time}", "placement_delay_pred": "0.0", "post_name": "", "post_status": "processing", "post_type": "", "prep_time": "15", "prep_time_pred": "1.0", "rendering_details": [ { "currency": "rupees", "display_text": "Item Total", "hierarchy": 1, "info_text": "", "intermediateText": "", "key": "item_total", "type": "display", "value": "300.00" }, { "currency": "rupees", "display_text": "GST", "hierarchy": 1, "info_text": "", "intermediateText": "", "key": "gst", "type": "display", "value": "15.00" }, { "currency": "rupees", "display_text": "Delivery Charges", "hierarchy": 1, "intermediateText": "", "key": "delivery_charges", "type": "display", "value": "0.00" } ], "restaurant_address": "Shop No. 1225, 26th Main Road, 9th Block, Jayanagar, Bengaluru, Karnataka", "restaurant_area_code": "${restaurant_area_code}", "restaurant_area_name": "Jayanagar", "restaurant_city_code": "${restaurant_city_code}", "restaurant_city_name": "Bangalore", "restaurant_commission_exp": "[bill_without_taxes]*0.05", "restaurant_cover_image": "ywp8ph9lioyafhfjfaas", "restaurant_coverage_area": "jayanagar", "restaurant_cuisine": [ "Ice Cream", "Desserts" ], "restaurant_customer_distance": "1", "restaurant_discount_hit": 0, "restaurant_email": "cornerhouseme@gmail.com", "restaurant_has_inventory": "0", "restaurant_id": "${restaurant_id}", "restaurant_lat_lng": "${restaurant_lat_lng}", "restaurant_locality": "9th Block", "restaurant_mobile": "9880594012", "restaurant_name": "Corner House Ice Cream", "restaurant_new_slug": "corner-house-ice-cream-9th-block-jayanagar", "restaurant_payment_mode": { "agreement_type": "1", "agreement_type_string": "Postpaid" }, "restaurant_phone_numbers": "9008613367", "restaurant_taxation_type": "GST", "restaurant_type": "D", "service_tax_on_commission": "0", "sharedOrder": false, "sid": "8hr8613b-4335-4e8e-8b5c-7d5dfebafdf7", "sla": "45", "sla_difference": "0", "sla_max": "60", "sla_min": "30", "sla_time": "50", "special_fee": 0, "start_banner_factor": "1.0", "stop_banner_factor": "2.5", "success_message": "Your order will be delivered shortly. Keep Swiggying!", "success_title": "Yay! Order Received", "swiggy_discount_hit": 0, "swiggy_money": 0, "swuid": "fa81c642-9852-461b-ab13-4caf03a8f496", "tax_expressions": { "CGST": "[CART_SUBTOTAL_WITHOUT_PACKING]*0.025", "IGST": "[CART_SUBTOTAL_WITHOUT_PACKING]*0.0", "item_GST_inclusive": false, "packaging_CGST": "[TOTAL_PACKING_CHARGES]*0.0", "packaging_GST_inclusive": false, "packaging_IGST": "[TOTAL_PACKING_CHARGES]*0.0", "packaging_SGST": "[TOTAL_PACKING_CHARGES]*0.0", "Service Charges": "[CART_SUBTOTAL]*0", "Service Tax": "[CART_SUBTOTAL]*0", "service_charge_CGST": "[SERVICE_CHARGE]*0.0", "service_charge_GST_inclusive": false, "service_charge_IGST": "[SERVICE_CHARGE]*0.0", "service_charge_SGST": "[SERVICE_CHARGE]*0.0", "SGST": "[CART_SUBTOTAL_WITHOUT_PACKING]*0.025", "Vat": "[CART_SUBTOTAL]*0" }, "TCS_on_bill": { }, "TCS_on_bill_expressions": { "CTCS": "0", "ITCS": "0", "STCS": "0" }, "threshold_fee": 0, "tid": "f56b9466-8d2a-4637-aff2-39c2935b1af7", "time_fee": 0, "trade_discount": 0, "trade_discount_meta": [ ], "type_of_partner": 2, "with_de": true }`
		//conn, err := amqp.Dial("amqp://admin:admin@rabbitmq.perfus.swiggyops.de:5672/")
		//failOnError(err, "Failed to connect to RabbitMQ")
		//defer conn.Close()
		//ch, err := conn.Channel()
		//IfailOnError(err, "Failed to open a channel")
		//defer ch.Close()
		restaurantlatlong := line[3]+","+line[4]
		//location, err := time.LoadLocation("IST")
		loc, _ := time.LoadLocation("Asia/Kolkata")
		now := time.Now().In(loc)
		day := now.Day()
		hour := now.Hour()
		min := now.Minute()
		orderPrefix := strconv.Itoa(day) + strconv.Itoa(hour) + strconv.Itoa(min)+"_"
		currentTime := time.Now().In(loc)
		r := strings.NewReplacer("${orderId}", "Bat1_"+orderPrefix+strconv.Itoa(count), "${time}", currentTime.Format("2006-01-02 15:04:05"), "${restaurant_id}", line[0], "${restaurant_area_code}", line[5], "${custlat}", line[1], "${custlon}", line[2], "${restaurant_city_code}", "1", "${restaurant_lat_lng}", restaurantlatlong)
		message := r.Replace(template)
		if count%100 == 0 {
			println(count)
		}
		//println(message)
		time.Sleep(1 * time.Millisecond)
		publishMessageInKafka("swiggy.orders",message)
		publishMessageInKafka("swiggy.orders_delivery",message)
		//publishMessage(ch, message, "", "dc.orders_delivery")
		//publishMessage(ch, message, "swiggy.orders","")
		count += 1
	}
	return count
}