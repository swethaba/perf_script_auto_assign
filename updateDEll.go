package main

import (
	"fmt"
	"math/rand"
	"strings"
	"bufio"
	"github.com/go-redis/redis"
	"log"
	"os"
	"time"
)

type LatLong struct {
	lat string
	lng string
}

type RedisClient struct {
	client redis.Client
}

func main() {
	Address := "perf-redis-location-0.swiggyperf.in"

	client := redis.NewClient(&redis.Options{
		Addr:     Address + ":6379",
		Password: "", // no password set
		DB:       1,  // use default DB
	})

	pong, err := client.Ping().Result()
	fmt.Println(pong, err)
	//	Address = "perf-redis-delivery-0.swiggyperf.in"

	//	client = redis.NewClient(&redis.Options{
	//		Addr:     Address + ":6379",
	//		Password: "", // no password set
	//		DB:       1,  // use default DB
	//	})


	zoneLatLongMap := getZoneLatLongMap()

	// active DEs from perf sql
	file, err := os.Open("/home/centos/goworkspace/src/bitbucket.org/swigy/gotdrogon/deid_zone.csv")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	count := 0
	now := time.Now().Add(((5 * 60) + 30) * time.Minute)
	time := now.Format("2006-01-02 15:04:05")
	for scanner.Scan() {
		if count%100 == 0 {
			fmt.Println(count)
		}
		t := scanner.Text()
		fields := strings.Fields(t)
		deId := fields[0]
		zone := fields[1]

		zoneLatLongList := zoneLatLongMap[zone]
		if len(zoneLatLongList) == 0 {
			continue
		}
		latLong := zoneLatLongList[rand.Intn(len(zoneLatLongList))]

		DLSLocation := "{\"trackable\":\"1\",\"lng\":\"" + latLong.lng + "\",\"image_url\":\"https://de-docs.s3.amazonaws.com/photographs/216.vnr\",\"name\":\"Test-vaishak\",\"mobile\":\"7022251082\",\"last_seen_at\":\""+time+"\",\"lat\":\"" + latLong.lat + "\"}"
		//              readFromRedis(*client, "foo")
		writeToRedis(*client, "DLS:DE_CURRENT_LOC_"+deId, DLSLocation)
		writeToRedis(*client, "DE_Location_"+deId, DLSLocation)
		//		fmt.Println("DLS:DE_CURRENT_LOC_"+deId)
		// writeToRedis(*client, "DE_TID_"+deId, "asasasasasasasasa")
		count += 1
	}
}

// create zone to latlong map from prod assignment run
func getZoneLatLongMap() map[string][]LatLong {
	file, err := os.Open("/home/centos/goworkspace/src/bitbucket.org/swigy/gotdrogon/depingwithzone.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	var m map[string][]LatLong
	m = make(map[string][]LatLong)
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		t := scanner.Text()
		fields := strings.Split(t, ",")
		lat := fields[1]
		lng := fields[2]
		zone := fields[3]
		latlong := LatLong{lat: lat, lng: lng}
		if _, ok := m[zone]; ok {
			m[zone] = append(m[zone], latlong)
		} else {
			m[zone] = append(m[zone], latlong)
		}
	}
	return m
}

func createNewRedisClient(Address string) redis.Client {
	client := redis.NewClient(&redis.Options{
		Addr:     Address + ":6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	pong, err := client.Ping().Result()
	fmt.Println(pong, err)
	// Output: PONG <nil>
	return *client
}

func writeToRedis(client redis.Client, key string, value string) {
	err := client.Set(key, value, 0).Err()
	if err != nil {
		panic(err)
	}
}

func readFromRedis(client redis.Client, keyName string) {
	val, err := client.Get(keyName).Result()
	fmt.Println(val)
	if err != nil {
		panic(err)
	}
	fmt.Println(keyName, val)

	val2, err := client.Get("key2").Result()
	if err == redis.Nil {
		fmt.Println("key2 does not exist")
	} else if err != nil {
		panic(err)
	} else {
		fmt.Println("key2", val2)
	}
}
